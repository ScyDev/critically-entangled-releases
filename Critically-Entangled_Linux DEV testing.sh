#!/bin/bash
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

./Critically-Entangled_Linux.sh --fullscreen=false --allowLan=true --configFile=defaults-DEV.ini
