#!/bin/bash
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

DATE_WITH_TIME=`date "+%Y-%m-%d_%H:%M:%S_%3N"`

./bin/CE_launcher.x86_64 -d --launchClient=true --memFixDMSX=1024 $* 2>&1 | tee "$SCRIPTPATH/log_$DATE_WITH_TIME.log"
