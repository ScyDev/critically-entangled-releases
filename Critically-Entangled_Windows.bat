@echo off

REM Absolute path this script is in
set SCRIPTPATH=%~dp0
echo switch to dir "%SCRIPTPATH%"
cd /D "%SCRIPTPATH%"

set SAVESTAMP=%DATE:/=-%_%TIME::=-%
set SAVESTAMP=%SAVESTAMP: =%
set SAVESTAMP=%SAVESTAMP:,=.%
echo timestamp %SAVESTAMP%

echo run game
"%SCRIPTPATH%\bin\CE_launcher.exe" -d --launchClient=true --memFixDMSX=1024 %*  > "%SCRIPTPATH%\log_%SAVESTAMP%.log" 2>&1
