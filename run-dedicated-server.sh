#!/bin/bash
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in
SCRIPTPATH=$(dirname "$SCRIPT")
cd "$SCRIPTPATH"

# Godot actually ignore the --main-pack param below and just wants the .pck file to have the same name as the executable
#cp Critically_Entangled_test.pck Godot_v3.1-stable_linux_server.pck
# using a link instead of this

# using the Godot server build. 
# for some reason we can't use the -d param in combination with custom command line args?
script /tmp/godot_server.log
./Critically_Entangled_test.x86_64 --fullscreen=false --testingMode=true --dedicatedServer=true --maxPlayers=1



# to see everything in log file except Godots ERROR spam
# cat /tmp/godot_server.log | grep -v -e "ERROR:" -e "At:"

